<?php

namespace App\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use LaravelZero\Framework\Commands\Command;
use Amp\Parallel\Worker;
use Amp\Promise;

use Wa72\HtmlPageDom\HtmlPage;
use function PHPUnit\Framework\stringContains;
use function Termwind\{render};

class ParseCommand extends Command
{
    protected $signature = 'parse';
    protected $description = '';


    public function handle()
    {
        try {
            $this->info('getting main page');
            $mainPage = $this->getMainPage();
            if (!$mainPage) {
                Log::error('Cant get the main page');
                return;
            }

            $this->info('getting count of pages');
            $countOfPages = $this->getCountOfPages($mainPage);
            if (!$countOfPages) {
                Log::error('Cant get count of pages');
                return;
            }

            $this->info('setting urls');
            $urls = $this->setURLsArray($countOfPages);

            $this->info('getting cars info');
            $carsInfo = $this->getCarsInfo($urls);

            $this->info('saving to csv');
            $this->saveToCsv($carsInfo);

            $this->info('get positive cars');
            $cars = $this->getPositiveCars($carsInfo);


            $keys = $values = $prices = [];
            $this->warn('printing result');
            foreach ($cars as $car) {
                $keys = array_keys($car);
                $values[] = array_values($car);
                $prices[] = $car['price'];
            }

            $this->table($keys, $values);
            $this->warn('calculating average');
            $average = array_sum($prices)/count($prices);
            dump($average);
            $this->warn($average);
        } catch (\Throwable $throwable) {
            Log::error($throwable);
        }
    }

    public function getMainPage(): bool|string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('SITE_URL'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $page = curl_exec($ch);
        curl_close($ch);

        return $page;
    }

    public function getCountOfPages(string $page): int
    {
        $page = new HtmlPage($page);
        $paginatorValue = $page->filter('.paginationselect :selected')->text();
        $unparsedCounter = $page->filter('.motorTable_fromToPagebar')->text();
        $countOfCars = trim(explode('/', $unparsedCounter)[1]);

        return ceil($countOfCars / $paginatorValue);
    }

    public function setURLsArray(int $countOfPages): array
    {
        $urls = [];
        $urls[] = env('SITE_URL');

        for ($numberOfPage = 2; $numberOfPage <= $countOfPages; $numberOfPage++) {
            $urls[] = env('SITE_URL').'/page' . $numberOfPage . '?sort=mark_model.asc';
        }

        return $urls;
    }

    public function getCarsInfo(array $urls): array
    {
        $carsInfo = $promises = [];

        foreach ($urls as $url) {
            $promises[$url] = Worker\enqueueCallable('file_get_contents', $url);
        }

        $responses = Promise\wait(Promise\all($promises));

        foreach ($responses as $url => $response) {
            $pageCars = [];
            $page = new HtmlPage($response);

            $carFullNames = $page->filter('.car-item .item-title span')->getCombinedText();

            $carNames = explode('...', $carFullNames);

            $carShortNamesWithId = $page->filter('.car-item .item-title .text-muted')->getCombinedText();
            $carIdNames = explode('#', $carShortNamesWithId);

            foreach ($carIdNames as $i => $carIdName) {
                if (!empty(trim($carIdName))) {
                    $id = explode(' - ', $carIdName)[0];
                    $name = explode(' - ', $carIdName)[1];

                    $pageCars[$i]['id'] = $id;
                    $pageCars[$i]['url'] = 'https://ecarstrade.com/cars/'.$id;
                    $pageCars[$i]['car_name'] = trim($name);
                    $pageCars[$i]['car_full_name'] = trim($carNames[$i - 1]);
                    $pageCars[$i]['price'] = rand(0, 1000000);
                }
            }

            $carsFeatures = $page->filter('.car-item .item-feature')->getCombinedText();

            $dirtyInfo = preg_split('/\n/', $carsFeatures);

            $emptyLineCounter = 0;
            $lineCounter = 0;
            $carCounter = 1;

            $attributeMap = [
                0 => 'first_reg',
                1 => 'mileage',
                2 => 'transmission',
                3 => 'fuel_type',
                4 => 'engine_volume',
                5 => 'power',
                6 => 'engine_class',
                7 => 'co2',
                8 => 'stock',
                9 => 'country',
                10 => 'empty',
                11 => 'vat',
                12 => 'non-use',
                13 => '?',
            ];

            foreach ($dirtyInfo as $i => $info) {
                if ($emptyLineCounter === 3) {
                    $carCounter++;
                    $lineCounter = 0;
                    $emptyLineCounter = 0;
                }

                if (!empty($info)) {
                    if (array_key_exists($lineCounter, $attributeMap)) {
                        if ($lineCounter === 1) {
                            $info = str_replace(' ', '', trim(explode('KM', $info)[0]));
                        }
                        $pageCars[$carCounter][$attributeMap[$lineCounter]] = trim($info);
                        $lineCounter++;
                        $emptyLineCounter = 0;
                    }
                }
                $emptyLineCounter++;
            }

            $carsInfo[] = $pageCars;
        }

        return array_merge(...$carsInfo);
    }

    public function saveToCsv(array $info): void
    {
        $file = fopen('cars_'. Carbon::now()->toString() .'.csv', 'w');

        foreach ($info as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }


    public function getPositiveCars(array $info) {
        Log::info('Use the list collected on previous step to find an average price of Mercedes A 180 + first registration is in 2019 + millage is between 60 000 and 120 000 km');
        $result = [];
        foreach ($info as $car) {
            if (isset($car['car_name']) && $car['car_name'] == 'Mercedes A 180'
                && $car['mileage'] > 60000
                && $car['mileage'] < 120000
            ) {
                $date = Carbon::createFromFormat('m/Y', $car['first_reg']);

                if ($date->year == 2019) {
                    $result[] = $car;
                }

            }
        }

        return $result;
    }
}
